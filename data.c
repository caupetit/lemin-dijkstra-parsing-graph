/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 17:46:28 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/23 17:49:56 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"
#include "libft.h"

int			data_error(void)
{
	ft_putendl_fd("ERROR", 2);
	return (0);
}

int			data_command_get(t_data *data, t_autm *autm, t_lnode *new)
{
	if (autm->status == command && autm->command == cstart)
	{
		if (data->start)
			return (0);
		data->start = &new->node;
	}
	if (autm->status == command && autm->command == cend)
	{
		if (data->end)
			return (0);
		data->end = &new->node;
	}
	return (1);
}

void		data_add_son(t_lnode **node, t_lnode **son)
{
	t_lnode	*tmp;
	t_lnode	*new;

	if (!(new = (t_lnode *)malloc(sizeof(t_lnode))))
		return ;
	new->node = (*son)->node;
	new->next = NULL;
	if (!(*node)->node.son)
		(*node)->node.son = new;
	else
	{
		tmp = (*node)->node.son;
		while (tmp && tmp->next)
			tmp = tmp->next;
		if (!tmp)
			tmp = new;
		else
			tmp->next = new;
	}
}

int			data_init(t_data *d)
{
	d->ant = 0;
	d->start = NULL;
	d->end = NULL;
	d->way = NULL;
	d->l_buf = hach_ltab_new();
	return (1);
}
