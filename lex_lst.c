/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_lst.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:29:20 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/17 17:07:11 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"
#include "libft.h"

void		lex_lst_del(t_lex **l_lex)
{
	t_lex	*tmp;

	while (*l_lex)
	{
		tmp = *l_lex;
		*l_lex = (*l_lex)->next;
		free(tmp->word);
		tmp->word = NULL;
		free(tmp);
		tmp = NULL;
	}
}

t_lex		*lex_lst_new(t_autl *autl)
{
	t_lex	*new;

	new = (t_lex *)malloc(sizeof(t_lex));
	new->type = autl->status;
	new->word = ft_strdup(autl->buf);
	new->next = NULL;
	return (new);
}

void		lex_lst_put(t_lex *lex)
{
	t_lex	*tmp;
	int		i;

	i = 0;
	tmp = lex;
	while (tmp)
	{
		i++;
		ft_printf("nro: %d", i);
		ft_printf(" test lex: %s", tmp->word);
		ft_printf(" type: %d\n", tmp->type);
		tmp = tmp->next;
	}
}

void		lex_add_token(t_autl *autl)
{
	t_lex	*new;
	t_lex	*tmp;

	if (!autl->buf)
		return ;
	tmp = autl->l_lex;
	if (autl->buf)
		new = lex_lst_new(autl);
	ft_bzero(autl->buf, ft_strlen(autl->buf));
	autl->i = 0;
	if (!tmp)
		autl->l_lex = new;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}
