#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/30 15:52:06 by caupetit          #+#    #+#              #
#    Updated: 2014/03/23 15:04:26 by caupetit         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

.PHONY: clean fclean all re

NAME = lem-in
SRC = main.c \
	data.c \
	lex_lst.c \
	lex_automaton.c \
	lex_func.c \
	map_automaton.c \
	map_func.c \
	hach.c \
	dij.c \
	way.c

OBJ = $(SRC:.c=.o)
FLAGS = -Wall -Wextra -Werror
CC = cc

all: lib $(NAME)

$(NAME): $(OBJ)
	@$(CC) $(FLAGS) $^ -o $@ -L./libft/ -lft
	@echo "==> Program [$@] compiled"

%.o: %.c
	@$(CC) $(FLAGS) -c $< -I libft/includes
	@echo "==> [$<] compiled"

lib:
	@make -C libft all

clean:
	@rm -f $(OBJ)
	@make -C libft clean
	@echo "==> Objects removed"
fclean: clean
	@rm -f $(NAME)
	@make -C libft fclean
	@echo "==> Program [$(NAME)] removed"

re: fclean all
