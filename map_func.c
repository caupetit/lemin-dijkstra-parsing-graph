/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_func.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 17:39:35 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/14 11:35:20 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

int			map_start(t_data *data, t_autm *autm, t_autl *autl)
{
	int		type;

	type = autl->l_lex->type;
	if (type == comment)
		return (1);
	if (type == number && ft_lstlen((t_list *)autl->l_lex) == 1)
	{
		data->ant = ft_atoi(autl->l_lex->word);
		autm->status = node;
		return (1);
	}
	return (0);
}

int			map_node(t_data *data, t_autm *autm, t_autl *autl)
{
	t_lex	*tmp;

	tmp = autl->l_lex;
	if (tmp->type == comment)
		return (1);
	if (tmp->type == command)
		return (map_cmd_get(autm, autl));
	if (ft_lstlen((t_list *)autl->l_lex) != 3)
		return (0);
	if (tmp->next->type == pipe)
		return (map_add_pipe(data, autm, autl));
	if (tmp->next->type != number || tmp->next->next->type != number)
		return (0);
	if (tmp->type == word || tmp->type == number)
		return (map_add_node(data, autm, autl));
	else
		return (0);
}

int			map_command(t_data *data, t_autm *autm, t_autl *autl)
{
	t_lex	*tmp;

	tmp = autl->l_lex;
	if (ft_lstlen((t_list *)autl->l_lex) == 1)
	{
		if (autl->l_lex->type == comment)
			return (1);
	}
	if (ft_lstlen((t_list *)autl->l_lex) != 3)
		return (0);
	if (tmp->next->type == pipe)
	{
		autm->status = pipe;
		return (map_add_pipe(data, autm, autl));
	}
	if (tmp->next->type != number || tmp->next->next->type != number)
		return (0);
	if (!map_add_node(data, autm, autl))
		return (0);
	autm->status = node;
	return (1);
}

int			map_pipe(t_data *data, t_autm *autm, t_autl *autl)
{
	if (ft_lstlen((t_list *)autl->l_lex) == 1)
	{
		if (autl->l_lex->type == comment || autl->l_lex->type == command)
			return (1);
	}
	if (ft_lstlen((t_list *)autl->l_lex) == 3)
	{
		if (autl->l_lex->next->type == pipe)
			return (map_add_pipe(data, autm, autl));
	}
	return (0);
}

int			map_end(t_autm *autm, t_autl *autl)
{
	lex_lst_del(&autl->l_lex);
	if (autm->status != pipe)
		return (0);
	autm->status = end;
	return (1);
}
