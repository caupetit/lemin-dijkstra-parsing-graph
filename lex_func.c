/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_func.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:29:20 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/23 18:04:40 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

int			lex_number(char c, t_autl *autl)
{
	if (ft_isspace(c))
	{
		lex_add_token(autl);
		autl->status = space;
		return (1);
	}
	if (c == '-')
	{
		lex_add_token(autl);
		autl->status = pipe;
		autl->buf[autl->i] = c;
		autl->i += 1;
		return (1);
	}
	if (!ft_isdigit(c) && c)
		autl->status = word;
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}

int			lex_word(char c, t_autl *autl)
{
	if (ft_isspace(c))
	{
		lex_add_token(autl);
		autl->status = space;
		return (1);
	}
	if (c == '-')
	{
		lex_add_token(autl);
		autl->status = pipe;
		autl->buf[autl->i] = c;
		autl->i += 1;
		return (1);
	}
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}

int			lex_pipe(char c, t_autl *autl)
{
	lex_add_token(autl);
	if (!c || c == 'L' || c == '-' || c == '#' || ft_isspace(c))
		return (0);
	if (ft_isdigit(c))
		autl->status = number;
	else
		autl->status = word;
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}

int			lex_space(char c, t_autl *autl)
{
	if (c == 'L' || c == '-' || c == '#' || !c)
		return (0);
	if (ft_isspace(c))
		return (1);
	if (ft_isdigit(c))
		autl->status = number;
	else
		autl->status = word;
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}
