/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:13:35 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/23 15:13:37 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

int			main(void)
{
	t_data	data;

	if (!data_init(&data))
		return (0);
	if (!map_get(&data))
		return (0);
	data.start->dist = 0;
	if (!dijkstra(&data, data.start))
		return (data_error());
	ft_printf("\n");
	dij_get_way(&data);
	way_set(&data);
	return (0);
}
