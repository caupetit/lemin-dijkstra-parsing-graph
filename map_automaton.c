/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_automaton.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 17:46:28 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/15 12:55:52 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

int			map_cmd_get(t_autm *autm, t_autl *autl)
{
	if (ft_strequ(autl->l_lex->word, "##start"))
		autm->command = cstart;
	else if (ft_strequ(autl->l_lex->word, "##end"))
		autm->command = cend;
	else
		return (1);
	autm->status = command;
	return (1);
}

int			map_add_pipe(t_data *data, t_autm *autm, t_autl *autl)
{
	t_lnode	**in;
	t_lnode	**add;

	if (!data->start || !data->end)
		return (0);
	if (!(in = hach_get_node(data, autl->l_lex->word)))
		return (0);
	if (!(add = hach_get_node(data, autl->l_lex->next->next->word)))
		return (0);
	if (!*in || !*add)
		return (0);
	data_add_son(in, add);
	data_add_son(add, in);
	autm->status = pipe;
	return (1);
}

int			map_add_node(t_data *data, t_autm *autm, t_autl *autl)
{
	static int	nb_nd;
	t_lnode		**tmp;
	t_lnode		*new;

	tmp = hach_get_node(data, autl->l_lex->word);
	if (tmp && *tmp)
		return (0);
	new = hach_lnode_new(autl->l_lex->word);
	*tmp = new;
	nb_nd++;
	if (nb_nd == SIZE_BUF / 2)
		hach_ltab_add(data);
	if (!data_command_get(data, autm, new))
		return (0);
	return (1);
}

int			map_auto(t_data *data, t_autm *autm, char *line)
{
	int			i;
	t_autl		autl;
	t_mapftab	ftab[MAP_STATUS_NB] =
	{
		{start, map_start},
		{node, map_node},
		{command, map_command},
		{pipe, map_pipe}
	};

	i = 0;
	if (!lex_line(line, &autl))
		return (map_end(autm, &autl));
	while (ftab[i].type != autm->status)
		i++;
	if (!ftab[i].f(data, autm, &autl))
		return (map_end(autm, &autl));
	ft_printf("%s\n", line);
	lex_lst_del(&autl.l_lex);
	return (1);
}

int			map_get(t_data *data)
{
	char	*line;
	t_autm	autm;
	int		ret;

	line = NULL;
	autm.status = start;
	autm.command = 0;
	ret = -1;
	while (autm.status != end && (ret = ft_get_next_line(0, &line)) > 0)
	{
		if (!line || !map_auto(data, &autm, line))
		{
			ft_strdel(&line);
			return (data_error());
		}
		ft_strdel(&line);
	}
	if (!ret && (!data->start || !data->end))
		return (data_error());
	return (1);
}
