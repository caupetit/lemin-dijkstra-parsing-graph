/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_automaton.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:29:20 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/23 18:03:24 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

int			lex_auto(char c, t_autl *autl)
{
	int			i;
	t_lexftab	ftab[LEX_TOKENS_NB] =
	{
		{start, lex_start},
		{comment, lex_comment},
		{command, lex_command},
		{number, lex_number},
		{word, lex_word},
		{pipe, lex_pipe},
		{space, lex_space}
	};

	i = 0;
	while (ftab[i].type != autl->status)
		i++;
	if (!ftab[i].f(c, autl))
		return (0);
	return (1);
}

int			lex_line(char *line, t_autl *autl)
{
	int		i;

	autl->buf = ft_strnew(ft_strlen(line));
	autl->l_lex = NULL;
	autl->i = 0;
	autl->status = start;
	i = -1;
	while (line[++i])
	{
		if (!lex_auto(line[i], autl))
			return (0);
	}
	if (!lex_auto(line[i], autl))
		return (0);
	if (autl->buf)
		lex_add_token(autl);
	ft_strdel(&autl->buf);
	return (1);
}

int			lex_start(char c, t_autl *autl)
{
	if (!c || c == 'L' || c == '-')
		return (0);
	if (ft_isspace(c))
		return (1);
	if (c == '#')
		autl->status = comment;
	else if (ft_isdigit(c))
		autl->status = number;
	else
		autl->status = word;
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}

int			lex_comment(char c, t_autl *autl)
{
	if (autl->i == 1 && c == '#')
		autl->status = command;
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}

int			lex_command(char c, t_autl *autl)
{
	autl->buf[autl->i] = c;
	autl->i += 1;
	return (1);
}
