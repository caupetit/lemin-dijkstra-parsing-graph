/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dij.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 15:57:53 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/23 16:07:04 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"
#include "libft.h"

int			dij_get_buf_best(t_lnode *htmp, t_node **found, int dist)
{
	while (htmp)
	{
		if (!htmp->node.made && htmp->node.dist != -1
			&& htmp->node.dist <= dist)
		{
			*found = &htmp->node;
			dist = htmp->node.dist;
		}
		htmp = htmp->next;
	}
	return (dist);
}

t_node		*dij_get_best_dist(t_data *data)
{
	int		i;
	int		dist;
	t_node	*ret;
	t_lbuf	*tmp;

	dist = 2147483647;
	ret = NULL;
	tmp = data->l_buf;
	while (tmp)
	{
		i = -1;
		while (++i < SIZE_BUF)
		{
			if ((tmp->ltab)[i])
				dist = dij_get_buf_best((tmp->ltab)[i], &ret, dist);
		}
		tmp = tmp->next;
	}
	return (ret);
}

int			dijkstra(t_data *data, t_node *start)
{
	t_node	*node;
	t_node	*son;
	t_lnode	*sons;

	node = start;
	while (node && !ft_strequ(node->name, data->end->name))
	{
		if (!(node = dij_get_best_dist(data)))
			return (0);
		node->made = 1;
		sons = node->son;
		while (sons)
		{
			son = &(*hach_get_node(data, sons->node.name))->node;
			if (!son->made && (son->dist == -1 || node->dist + 1 < son->dist))
			{
				son->dist = node->dist + 1;
				son->last = node->name;
			}
			sons = sons->next;
		}
	}
	return (1);
}

void		dij_get_way(t_data *data)
{
	int		i;
	t_way	**way;
	t_node	*node;

	i = 1;
	node = data->end;
	while (!ft_strequ(node->last, data->start->name))
	{
		node = &(*hach_get_node(data, node->last))->node;
		i++;
	}
	way = (t_way **)malloc(i * sizeof(t_way *));
	data->nb_nd_way = i;
	node = data->end;
	while (--i >= 0)
	{
		way[i] = (t_way *)malloc(sizeof(t_way));
		way[i]->name = node->name;
		way[i]->ant = 0;
		node = &(*hach_get_node(data, node->last))->node;
	}
	data->way = way;
}
