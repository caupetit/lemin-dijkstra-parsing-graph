/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hach.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 17:46:28 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/15 12:17:45 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lemin.h"
#include "libft.h"

void		hach_ltab_add(t_data *data)
{
	t_lbuf	*tmp;

	tmp = data->l_buf;
	while (tmp)
		tmp = tmp->next;
	tmp = hach_ltab_new();
}

t_lnode		*hach_lnode_new(char *name)
{
	t_lnode	*new;

	new = (t_lnode *)malloc(sizeof(t_lnode));
	new->node.name = ft_strdup(name);
	new->node.last = NULL;
	new->node.dist = -1;
	new->node.made = 0;
	new->node.ant = 0;
	new->node.son = NULL;
	new->next = NULL;
	return (new);
}

int			hach_get_i(char *name)
{
	int				i;
	unsigned int	hi;

	i = -1;
	hi = 5381;
	while (name[++i])
		hi *= 33 + name[i];
	hi = hi % SIZE_BUF;
	return ((int)hi);
}

t_lnode		**hach_get_node(t_data *data, char *name)
{
	int		i;
	t_lbuf	*l_tmp;
	t_lnode	**l_node;

	l_node = NULL;
	i = hach_get_i(name);
	l_tmp = data->l_buf;
	while (l_tmp)
	{
		l_node = &l_tmp->ltab[i];
		if (l_node && *l_node && &(*l_node)->node)
		{
			while (*l_node && !ft_strequ((*l_node)->node.name, name))
				l_node = &(*l_node)->next;
			if (*l_node)
				return (l_node);
		}
		l_tmp = l_tmp->next;
	}
	return (l_node);
}

t_lbuf		*hach_ltab_new(void)
{
	t_lbuf	*new;
	int		i;

	i = -1;
	new = (t_lbuf *)malloc(sizeof(t_lbuf));
	new->ltab = (t_lnode **)malloc(SIZE_BUF * sizeof(t_lnode *));
	while (++i < SIZE_BUF)
		new->ltab[i] = NULL;
	new->next = NULL;
	return (new);
}
