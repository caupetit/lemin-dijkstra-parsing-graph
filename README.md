    /* ************************************************************************** */
    /*                                                                            */
    /*                                                        :::      ::::::::   */
    /*                                                      :+:      :+:    :+:   */
    /*                    LEMIN                           +:+ +:+         +:+     */
    /*                                                  +#+  +:+       +#+        */
    /*                                                +#+#+#+#+#+   +#+           */
    /*   By: caupetit <caupetit@student.42.fr>             #+#    #+#             */
    /*                                                    ###   ########.fr       */
    /*                                                                            */
    /* ************************************************************************** */                                         

# Welcome to LEMIN project. #
****
Works only on mac OSX. A simple implementation c standar functions is used. See [libft/](https://bitbucket.org/caupetit/lemin-dijkstra-parsing-graph/src/117b5e161703cbc56c74e5ea9737c2ad416524e4/libft/?at=master)

This program make ants traveling in a anthill from entrance to exit.

This program implements DIJKSTRA algorithm with this data structure:

- Anthill's rooms are loaded in HASH TABLE from standar input.

- Input is pars with a LEXER and AUTOMATON.

- Each room got a chained list of rooms she can communicate whit.
****
Use:

 - make

 - ./lem-in

then:

 - copy paste the Anthill bellow and press return.

or:

 - ./lem-in < map

 - ./lem-in < map1000.txt

- ./lem-in < map_hard


****

3

room1     3 3

room2     10 10

room3     43 43

\#(infinite number of room can be set)

room4     44 44


\##start

ENTRANCE  42 42


\##end

EXIT      24 24

room6     62 62

room2-room2

room3-ENTRANCE

room3-room2

room2-EXIT

EXIT-room4

\#                        (infinite number of tunnels can be set)


****


On first empty line or EOF, program try to travel ants if possible

- most pars errors should be managed.

- an anthill with no way between entrance and exit only give ERROR.

traveling is print on standar input on form:

L1-room3

L1-room2 L2-room3

L1-EXIT L2-room2 L3-room3

L2-EXIT L3-room2

L3-EXIT