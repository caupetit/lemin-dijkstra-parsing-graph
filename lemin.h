/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/17 15:29:20 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/23 15:01:57 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# define SIZE_BUF		4096
# define LEX_TOKENS_NB	7
# define MAP_STATUS_NB	4

/*
**	enums for map and lex automatons
*/
enum			e_map_auto
{
	start = 0, command, node, pipe
};

enum			e_lex_auto
{
	word = 4, number, comment, space, end
};

enum			e_cmd
{
	cend = 80, cstart
};

/*
**	graph node
*/
typedef struct	s_node
{
	char			*name;
	char			*last;
	int				dist;
	int				made;
	int				ant;
	struct s_lnode	*son;
}				t_node;

/*
**	chained lists for automatons and hach tab
*/
typedef struct	s_lex
{
	int				type;
	char			*word;
	struct s_lex	*next;
}				t_lex;

typedef struct	s_lnode
{
	t_node			node;
	struct s_lnode	*next;
}				t_lnode;

typedef struct	s_lbuf
{
	t_lnode			**ltab;
	struct s_lbuf	*next;
}				t_lbuf;

/*
**	structs fot automatons
*/
typedef struct	s_autm
{
	int		status;
	int		command;
}				t_autm;

typedef struct	s_autl
{
	int		status;
	int		i;
	char	*buf;
	t_lex	*l_lex;
}				t_autl;

/*
**	data struct
*/
typedef struct	s_way
{
	char	*name;
	int		ant;
}				t_way;

typedef struct	s_data
{
	int		ant;
	t_node	*start;
	t_node	*end;
	t_lbuf	*l_buf;
	t_way	**way;
	int		nb_nd_way;
}				t_data;

/*
**	functions pointers and associed structs for automatons
*/
typedef int	(*t_fu)(char, t_autl *);

typedef	int	(*t_mfu)(t_data *, t_autm *, t_autl *);

typedef struct	s_lexftab
{
	int		type;
	t_fu	f;
}				t_lexftab;

typedef struct	s_mapftab
{
	int		type;
	t_mfu	f;
}				t_mapftab;

/*
**	data.c
*/
int			data_error(void);
int			data_init(t_data *d);
void		data_add_son(t_lnode **node, t_lnode **son);
int			data_command_get(t_data *data, t_autm *autm, t_lnode *new);

/*
**	lex_lst.c
*/
t_lex		*lex_lst_new(t_autl *autl);
void		lex_lst_del(t_lex **l_lex);
void		lex_lst_put(t_lex *lex);
void		lex_add_token(t_autl *autl);

/*
**	lex_automaton.c
*/
int			lex_auto(char c, t_autl *autl);
int			lex_line(char *line, t_autl *autl);
int			lex_start(char c, t_autl *autl);
int			lex_comment(char c, t_autl *autl);
int			lex_command(char c, t_autl *autl);

/*
**	lex_func.c
*/
int			lex_number(char c, t_autl *autl);
int			lex_word(char c, t_autl *autl);
int			lex_pipe(char c, t_autl *autl);
int			lex_space(char c, t_autl *autl);

/*
**	map_automaton.c
*/
int			map_cmd_get(t_autm *autm, t_autl *autl);
int			map_add_pipe(t_data *data, t_autm *autm, t_autl *autl);
int			map_add_node(t_data *data, t_autm *autm, t_autl *autl);
int			map_auto(t_data *data, t_autm *autm, char *line);
int			map_get(t_data *data);

/*
**	map_fun.c
*/
int			map_start(t_data *data, t_autm *autm, t_autl *autl);
int			map_node(t_data *data, t_autm *autm, t_autl *autl);
int			map_command(t_data *data, t_autm *autm, t_autl *autl);
int			map_pipe(t_data *data, t_autm *autm, t_autl *autl);
int			map_end(t_autm *autm, t_autl *autl);

/*
**	hach.c
*/
t_lnode		**hach_get_node(t_data *data, char *name);
t_lnode		*hach_lnode_new(char *name);
t_lbuf		*hach_ltab_new(void);
void		hach_ltab_add(t_data *data);
int			hach_get_i(char *name);

/*
**	dij.c
*/
int			dij_get_buf_best(t_lnode *htmp, t_node **found, int dist);
t_node		*dij_get_best_dist(t_data *data);
int			dijkstra(t_data *data, t_node *start);
void		dij_get_way(t_data *data);

/*
**	way.c
*/
void		way_put(t_data *data);
void		way_set(t_data *data);

#endif /* !LEMIN_H */
