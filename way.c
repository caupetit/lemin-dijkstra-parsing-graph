/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   way.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 15:28:50 by caupetit          #+#    #+#             */
/*   Updated: 2014/02/23 18:02:46 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include "libft.h"

void		way_put(t_data *data)
{
	int		i;
	int		j;
	int		k;

	i = 0;
	k = 0;
	while (++i <= data->ant)
	{
		j = -1;
		while (++j < data->nb_nd_way)
		{
			if (data->way[j]->ant == i)
			{
				if (k)
					ft_printf(" ");
				ft_printf("L%d-", data->way[j]->ant);
				ft_printf("%s", data->way[j]->name);
				k = 1;
			}
		}
	}
}

void		way_set(t_data *data)
{
	int		i;
	int		j;

	i = 1;
	while (data->way[0]->ant <= data->ant + data->nb_nd_way)
	{
		j = -1;
		while (++j < data->nb_nd_way)
		{
			if (i < data->nb_nd_way)
			{
				if (j < i)
					(data->way)[j]->ant += 1;
			}
			else
				(data->way)[j]->ant += 1;
		}
		way_put(data);
		if (i < data->ant + data->nb_nd_way)
			ft_printf("\n");
		i++;
	}
}
